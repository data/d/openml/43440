# OpenML dataset: Forest-Fire-Area

https://www.openml.org/d/43440

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
The dataset contains 517 fires from the Montesinho natural park in Portugal. For each incident weekday, month, coordinates, and the burnt area are recorded, as well as several meteorological data such as rain, temperature, humidity, and wind. The workflow reads the data and trains a regression model based on the spatial, temporal, and weather variables.
Acknowledgements
All credit for this dataset goes to P. Cortez and A. Morais.
P. Cortez and A. Morais. A Data Mining Approach to Predict Forest Fires using Meteorological Data. In J. Neves, M. F. Santos and J. Machado Eds., New Trends in Artificial Intelligence, Proceedings of the 13th EPIA 2007 - Portuguese Conference on Artificial Intelligence, December, Guimaraes, Portugal, pp. 512-523, 2007. APPIA, ISBN-13 978-989-95618-0-9.
Burning Area Prediction

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43440) of an [OpenML dataset](https://www.openml.org/d/43440). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43440/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43440/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43440/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

